### Rodar o projeto
sam local start-api

### Rodar o projeto com debug na porta 5858
sam local start-api -d 5858

### Efetuar chamada usando CURL
curl http://localhost:3000?content=Hello!

### S3 Bucket Policy
    {
        "Version": "2012-10-17",
        "Id": "Policy1584987846274",
        "Statement": [
            {
                "Sid": "Stmt1584987843727",
                "Effect": "Allow",
                "Principal": {
                    "AWS": "arn:aws:iam::087203132684:user/app_deploy_dev"
                },
                "Action": "s3:ListBucket",
                "Resource": "arn:aws:s3:::sample.aws.serverless.bucket"
            },
            {
                "Sid": "Stmt15849878437279",
                "Effect": "Allow",
                "Principal": {
                    "AWS": "arn:aws:iam::087203132684:user/app_deploy_dev"
                },
                "Action": [
                    "s3:DeleteObject",
                    "s3:GetObject",
                    "s3:PutObject"
                ],
                "Resource": "arn:aws:s3:::sample.aws.serverless.bucket/*"
            }
        ]
    }