'use strict';

var AWS = require("aws-sdk");

var response;

exports.lambdaHandler = (event, context, callback) => {
    console.log("Starting GET Operation");
    console.log("This is the Event Object: " + JSON.stringify(event))
    console.log("This is the Context Object: " + JSON.stringify(event))
    
    var content = {
        "content": event && event.queryStringParameters && event.queryStringParameters.content ? 
                event.queryStringParameters.content : "empty content" 
    };

    response = {
        'statusCode': 200,
        'body': JSON.stringify(content)
    };

    console.log("Response will be: " + JSON.stringify(response));
    console.log("Finishing GET Operation");
    callback(null, response);
};